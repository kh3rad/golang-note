# goLang Note

golang note from Hands-On Full Stack Development with Go book

## Go Concurrency

In Go, concurrency means the ability of your program to cut itself into
smaller pieces, then the ability to run the different independent pieces at
different times, with the goal of executing all the tasks as quickly as possible
based on the amount of resources available.

## Goroutines

A goroutine is simply defined as a light-weight thread that you can use in
your program; it's not a real thread.You just append the go keyword
before the function that you would like to run concurrently. The syntax is
quite simple:

```
go somefunction()
```
Goroutines are very light from a memory and resources point of view.

## Go channels

An important question can now be addressed; what if we need to share a
piece of data between two different goroutines?
"Do not communicate by sharing memory; instead, share memory by communicating."
What does that mean?
Go prefers to communicate the data from onegoroutine to another. This communicate part is achieved through the Go
channels.

### Regular channels

To declare a channel in Go, you simply use the make keyword, as follows:
```
myChannel := make(chan int)
```
In the preceding code, we created and initialized a channel called
myChannel that can hold int values. This channel can then be used to send an
int value from one goroutine to another.
Here is how to receive a value from a channel:
```
//myIntValue will host the value received from the channel
myIntValue := <-myChannel
```
Here is how to send a value to a channel:
```
myChannel <- 4
```
- if you send a value via a channel but there is no
other goroutine waiting for it on the other end, your goroutine will block.
- if you are attempting to receive a value via a channel but
there is no other goroutine sending it on the other end, your goroutine will
block.

```
close(ch)
```
This syntax can be used to close a channel. Once a channel is closed, you
cannot send data to it anymore, otherwise, a **panic** will occur.

```
for {
    i, ok := <-ch
    if !ok {
        break
    }
    fmt.Println("Received value:", i)
}
```
The preceding line is a special syntax(i, ok := <-ch) to inspect whether a channel is
closed. If the channel is not closed, the value of ok will be true, while i will
get the value getting sent via the channel.

We can replace the preceding code with the following code:
```
for i := range ch {
    fmt.Println("Received value:", i)
}
```
The for..range syntax is allowed on channels as it allows you to keep receiving data from a channel until the channel gets closed.

### Buffered channels
A buffered channel is a special type of channel that holds a buffer that contains a number of items. Unlike a regular channel, a buffered channel doesn't block unless the following takes place:
- Its buffer is empty and we are trying to receive a value from the
channel.
- Its buffer is full and we are trying to send a value to the channel. 
To declare a buffered channel, we use the following syntax:
```
myBufferedChannel := make(chan int,10)
```
To send a value to the buffered channel, we use the same syntax as with regular channels.

## The select statement
The select statement is an important construct in Go. It allows you to control multiple channels at the same time. With select , you can send or receive values to different channels, and then execute code based on the channel that unblocks the first. The function generates a channel that only receives a value after the specified timeout
```
select {
    case i := <-ch1:
        fmt.Println("Received value on channel ch1:", i)
    case ch2 <- 10:
        fmt.Println("Sent value of 10 to channel ch2")
    default:
        fmt.Println("No channel is ready")
    case <-time.After(1 * time.Second):
        fmt.Println("timed out")
}
```
The time.After() function is very popular in Go, and especially in select statements.The preceding code will synchronize between three channels: ch1 , ch2 , and the time.After() channel.The select statement also supports the default case. The default case will execute immediately if none of the channels are ready;


If multiple channels finish at the same time while being controlled by a select statement, the channel case to be executed is picked at random.

## The sync package
The sync package is what you will use when you absolutely need to create a lock-in Go.
The word mutex, in the world of computer programming, refers to an object
that allows multiple threads to access the same resource (such as shared
memory). Mutex is so named because it allows only one thread to access
data at one time.

The workflow of a mutex in a piece of software typically works as follows:
1. A thread acquires the mutex
2. No other threads can acquire the mutex as long as one thread has it
3. The thread that acquired the mutex can access some resources without
any disturbance from the other threads
4. When its tasks are done, the thread that acquired the mutex releases the
mutex so that other threads can compete for it again

n Go, you make use of goroutines and not full threads. So, when you use
mutexes in Go, they will manage the resource access between goroutines.

### The simple mutex

In Go, a simple lock is a pointer to a mutex struct type, which belongs to the
sync package. We can create a mutex as follows:
```
var myMutex = &sync.Mutex{}
```
### The read-write mutex
Go also supports a read-write lock. A read-write lock differentiates between
read and write operations. So, whenever you only perform concurrent read
operations, the goroutines won't block. However, whenever you perform a
write operation, all other reads and writes get blocked until the write lock is
released.
```
var myRWMutex = &sync.RWMutex{}
```
To perform a read operation, we make use of the RLock() and RUnlock()
methods of the Go struct:
```
myRWMutex.RLock()
fmt.Println(myMap[1])
myRWMutex.RUnlock()
```
To perform a write operation, we make use of the Lock() and Unlock()
methods:
```
myRWMutex.Lock()
myMap[2] = 200
myRWMutex.Unlock()
```
The ```*sync.RWMutex``` type can be found all over the place in Go's standard
package.

## Wait groups
The concept of wait groups is very important for building production level
software in Go; it allows you to wait for multiple goroutines to finish before
you proceed with the rest of your code.

```
package main
import (
    "fmt"
    "sync"
)
// Create a global waitgroup:
var wg = &sync.WaitGroup{}
func main() {
    myChannel := make(chan int)
    //Increment the wait group internal counter by 2
    wg.Add(2)
    go runLoopSend(10, myChannel)
    go runLoopReceive(myChannel)
    //Wait till the wait group counter is 0
    wg.Wait()
}
func runLoopSend(n int, ch chan int) {
    //Ensure that the wait group counter decrements by one after //our function exits
    defer wg.Done()
    for i := 0; i < n; i++ {
        ch <- i
    }
    close(ch)
}
func runLoopReceive(ch chan int) {
    //Ensure that the wait group counter decrements after our
    defer wg.Done()
    for {
        i, ok := <-ch
        if !ok {
            break
        }
        fmt.Println("Received value:", i)
    }
}
```
